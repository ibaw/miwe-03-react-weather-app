import { useState } from "react";
import SearchBox from "./components/SearchBox";

const weatherApi = {
  key: "8827536ec2722a37f523fb089167b1d1",
  base: "https://api.openweathermap.org/data/2.5/",
};

const unsplashApi = {
  key: "GKA0EExUzdUuPGJZYRdPepGCiBNzfSmy5vx_uidRYOw",
  base: "https://api.unsplash.com/search/photos/",
};


function App() {
  const [query, setQuery] = useState("");
  const [weather, setWeather] = useState({});

  const [background, setBackground] = useState();

  const search = (evt) => {
    if (evt.key === "Enter") {
      fetch(
        `${weatherApi.base}weather?q=${query}&units=metric&APPID=${weatherApi.key}`
      )
        .then((res) => res.json())
        .then((result) => {
          setWeather(result);
          setQuery("");
          console.log(result);
        });

      fetch(`${unsplashApi.base}?client_id=${unsplashApi.key}&query=${query}`)
        .then((res) => res.json())
        .then((result) => {
          if (result?.results.length > 0) {
            setBackground(result.results[0].urls.regular);
          } else {
            setBackground();
          }
        });
    }
  };

  return (
    <>
          <div
      style={{ backgroundImage: `url(${background})` }}
      className={
        typeof weather.main != "undefined"
          ? weather.main.temp > 16
            ? "app warm"
            : "app"
          : "app"
      }
    >
      <main>
        <SearchBox
          onChange={(e) => setQuery(e.target.value)}
          value={query}
          onKeyPress={search}
        />
        {typeof weather.main != "undefined" ? (
          <div>
            <div className="location-box">
              <div className="location">
                {weather.name}, {weather.sys.country}
              </div>
              <div className="date">
                {new Date().toLocaleDateString("de-CH")}
              </div>
            </div>
            <div className="weather-box">
              <div className="temp">{Math.round(weather.main.temp)}°c</div>
              <div className="weather">{weather.weather[0].main}</div>
            </div>
          </div>
        ) : (
          ""
        )}
      </main>
    </div>
    </>
  )
}

export default App
